
# Load utility
. ".\local-utility.ps1"

# Check MSYS2 installation
$cmd = $MSYS2_FOLDER + "\msys2_shell.cmd"
if(![System.IO.File]::Exists($cmd)){
    Write-Host (">> {0} does not seem to be the root folder of an existing MSYS2 installation" -f $MSYS2_FOLDER)
    exit
}

# Change working path
$path = $PSScriptRoot.replace("\","/")
ExecuteMSYS2Cmd("cd " + $path)

# Update MSYS2
ExecuteMSYS2Cmd("pacman -Syuu --noconfirm")

# # Install dependencies
# ExecuteMSYS2Cmd("pacman -S base-devel --noconfirm")

# # Clone the code
# $remote_url = "https://github.com/libimobiledevice/libplist.git"
# $cmd = ("git clone --single-branch --branch master {0}" -f $remote_url)
# ExecuteCmd($cmd)

# Display all environment variables
gci env:* | sort-object name