

$Global:MSYS2_FOLDER = "C:\msys64"
# $Global:MSYS2_FOLDER = "D:\dev\msys64"
$Global:BASH_EXE = $MSYS2_FOLDER + "\usr\bin\bash.exe"

function ExecuteCmd($cmd)
{
    Write-Host (">> Execute {0}" -f $cmd)
    [Diagnostics.Process]::Start($cmd)
    Write-Host (">> Finish {0}" -f $cmd)
}

# MSYS2 (C:\msys64)
# Example: C:\msys64\usr\bin\bash.exe -l -c "pacman -Syuu"
function ExecuteMSYS2Cmd($cmd)
{
    Write-Host (">> Execute {0}" -f $cmd)

    $prms = ("-l -c '{0}'" -f $cmd)
    $prms = $prms.Split(" ")
    & "$BASH_EXE" $prms
    
    Write-Host (">> Finish {0}" -f $cmd)
}