
# Load utility
. ".\local-utility.ps1"

# Change working path
$path = $PSScriptRoot.replace("\","/")
ExecuteMSYS2Cmd("cd " + $path)
ExecuteMSYS2Cmd("pwd")

ExecuteMSYS2Cmd("./autogen.sh")
ExecuteMSYS2Cmd("make")
ExecuteMSYS2Cmd("make install")
ExecuteMSYS2Cmd("cd ..")